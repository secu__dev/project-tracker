FROM golang:alpine as build

LABEL maintainer="SecuDev <remy.chaix@gmail.com>"

WORKDIR $GOPATH/src/project-tracker

COPY projectTracker.go .
RUN mkdir quality \
 && mkdir security
COPY quality/*.go quality/
COPY security/*.go security/

RUN apk add --no-cache git mercurial \
 && go get -d -v ./... \
 && go install -v ./... \
 && apk del git mercurial

FROM alpine:latest

COPY --from=build /go/bin/project-tracker /usr/local/bin/project-tracker

ENTRYPOINT ["/usr/local/bin/project-tracker"]