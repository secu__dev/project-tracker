package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"project-tracker/quality"
	"project-tracker/security"
	"strings"
	"time"
)

func main() {
	var folder string
	var projectName string
	var projectVersion string

	flag.StringVar(&folder, "folder", "/data", "Folder to browse for data")
	flag.StringVar(&projectName, "project", "unknown", "Project name")
	flag.StringVar(&projectVersion, "version", string(time.Now().Unix()), "Project version")
	flag.Parse()

	if len(os.Args[1:]) != 3 {
		return
	}
	var analysisTime = time.Now()
	err := filepath.Walk(projectName,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}

			// json file, can be several data types
			if strings.HasSuffix(info.Name(), ".json") && strings.HasPrefix(info.Name(), "dependency-check") {
				data, err := ioutil.ReadFile(path)
				if err == nil {
					var result = security.ReadDependencyCheck(projectName, projectVersion, data)
					for i := 0; i < len(result); i++ {
						json.NewEncoder(os.Stdout).Encode(result[i])
					}
				} else {
					log.Println(err)
				}
			} else if info.Name() == "checkstyle.xml" || strings.HasSuffix(path, "/checkstyle/main.xml") {
				var result = quality.ReadCheckstyle(analysisTime, projectName, projectVersion, path)
				for i := 0; i < len(result); i++ {
					json.NewEncoder(os.Stdout).Encode(result[i])
				}
			} else if info.Name() == "pmd.xml" || strings.HasSuffix(path, "/pmd/main.xml") {
				fmt.Println("PMD detected, but not implemented yet")
			} else if info.Name() == "spotbugs.xml" || strings.HasSuffix(path, "/spotbugs/main.xml") {
				fmt.Println("Spotbugs detected, but not implemented yet")
			} else if strings.HasSuffix(info.Name(), ".json") && strings.HasPrefix(info.Name(), "anchore") {
				fmt.Println("Anchore detected, but not implemented yet")
			} else if strings.HasSuffix(info.Name(), ".json") && strings.HasPrefix(info.Name(), "anchore") {
				fmt.Println("Anchore detected, but not implemented yet")
			} else if info.Name() == "jacoco.xml" || strings.HasSuffix(path, "/jacoco/main.xml") {
				err, result := quality.ReadJacoco(projectName, projectVersion, path)
				if err == nil {
					for i := 0; i < len(result); i++ {
						json.NewEncoder(os.Stdout).Encode(result[i])
					}
				}
			}
			return nil
		})
	if err != nil {
		log.Println(err)
	}
}
