package quality

import (
	"github.com/phayes/checkstyle"
	"log"
	"time"
)

type CheckstyleSummary struct {
	AnalysisTime   time.Time `json:"analysis_time"`
	Tool           string    `json:"tool"`
	Project        string    `json:"project"`
	ProjectVersion string    `json:"project_version"`
	File           string    `json:"file"`
	Line           int       `json:"line"`
	Message        string    `json:"message"`
	TypeError      string    `json:"type_error"`
	Severity       string    `json:"severity"`
}

func ReadCheckstyle(analysisTime time.Time, projectName string, projectVersion string, filename string) []CheckstyleSummary {
	checkStyle, err := checkstyle.ReadFile(filename)
	if err != nil {
		log.Fatal(err)
		return nil
	}
	checkstyleIssuesList := make([]CheckstyleSummary, 0)
	for _, file := range checkStyle.File {
		for _, codingError := range file.Error {
			checkstyleIssuesList = append(checkstyleIssuesList, CheckstyleSummary{analysisTime,
				"Checkstyle",
				projectName,
				projectVersion,
				file.Name,
				codingError.Line,
				codingError.Message,
				codingError.Source,
				string(codingError.Severity),
			})
		}
	}
	return checkstyleIssuesList
}
