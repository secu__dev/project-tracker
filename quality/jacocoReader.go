package quality

import (
	"github.com/antchfx/xmlquery"
	"os"
	"strconv"
	"time"
)

type JacocoResult struct {
	AnalysisTime       time.Time `json:"analysis_time"`
	Tool               string    `json:"tool"`
	Project            string    `json:"project"`
	ProjectVersion     string    `json:"project_version"`
	CoverageType       string    `json:"coverage_type"`
	CoverageObjectType string    `json:"coverage_object_type"`
	CoverageObjectName string    `json:"coverage_object_name"`
	Missed             int       `json:"missed"`
	Covered            int       `json:"covered"`
}

func ReadJacoco(project string, projectVersion string, filename string) (error, []JacocoResult) {
	f, err := os.Open(filename)
	if err != nil {
		return err, nil
	}
	doc, err := xmlquery.Parse(f)
	if err != nil {
		return err, nil
	}

	jacocoResultList := make([]JacocoResult, 0)

	analysisTimeNode := xmlquery.FindOne(doc, "/report/sessioninfo/@start")
	var analysisTime time.Time
	if analysisTimeNode != nil {
		n, err := strconv.ParseInt(analysisTimeNode.InnerText(), 10, 64)
		if err == nil {
			analysisTime = time.Unix(n/1000, 0)
		} else {
			return err, nil
		}
	}
	list := xmlquery.Find(doc, "/report/counter")
	var missed int
	var covered int
	var typeCoverage string
	for _, data := range list {
		missed = -1
		covered = -1
		typeCoverage = ""
		for _, item := range data.Attr {
			if item.Name.Local == "type" {
				typeCoverage = item.Value
			} else if item.Name.Local == "missed" {
				missed, _ = strconv.Atoi(item.Value)
			} else if item.Name.Local == "covered" {
				covered, _ = strconv.Atoi(item.Value)
			}
		}
		jacocoResultList = append(jacocoResultList, JacocoResult{
			analysisTime,
			"Jacoco",
			project,
			projectVersion,
			typeCoverage,
			"PROJECT",
			"PROJECT",
			missed,
			covered,
		})
	}
	return nil, jacocoResultList
}
