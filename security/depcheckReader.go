package security

import (
	jsoniter "github.com/json-iterator/go"
	"time"
)

type VulnerabilitySummary struct {
	AnalysisTime   time.Time `json:"analysis_time"`
	Tool           string    `json:"tool"`
	Project        string    `json:"project"`
	ProjectVersion string    `json:"project_version"`
	DependencyName string    `json:"dependency_name"`
	Cve            string    `json:"cve"`
	Level          string    `json:"level"`
	Cpe            string    `json:"cpe"`
	VersionFix     string    `json:"version_fix"`
}

func ReadDependencyCheck(projectName string, projectVersion string, data []byte) []VulnerabilitySummary {
	var reportDate = jsoniter.Get(data, "projectInfo").Get("reportDate").ToString()
	var analysisTime time.Time
	var err error

	if reportDate == "" {
		analysisTime = time.Now()
	} else {
		analysisTime, err = time.Parse("2006-01-02T15:04:05.000Z", reportDate)
		if err != nil {
			analysisTime = time.Now()
		}
	}
	var dependenciesCount = jsoniter.Get(data, "dependencies").Size()
	var vulnCount int
	var vulnerableSoftwareCount int
	var currentVulnerability VulnerabilitySummary
	var listVulnerabilities = make([]VulnerabilitySummary, 0)

	for i := 0; i < dependenciesCount; i++ {
		currentVulnerability = VulnerabilitySummary{AnalysisTime: analysisTime,
			Tool:           "DependencyCheck",
			Project:        projectName,
			ProjectVersion: projectVersion,
			DependencyName: jsoniter.Get(data, "dependencies", i).Get("fileName").ToString()}
		if jsoniter.Get(data, "dependencies", i).Get("vulnerabilities").GetInterface() != nil {
			vulnCount = jsoniter.Get(data, "dependencies", i).Get("vulnerabilities").Size()
			for v := 0; v < vulnCount; v++ {
				currentVulnerability.Cve = jsoniter.Get(data, "dependencies", i).Get("vulnerabilities", v).Get("name").ToString()
				currentVulnerability.Level = jsoniter.Get(data, "dependencies", i).Get("vulnerabilities", v).Get("severity").ToString()
				vulnerableSoftwareCount = jsoniter.Get(data, "dependencies", i).Get("vulnerabilities", v).Get("vulnerableSoftware").Size()
				for vs := 0; vs < vulnerableSoftwareCount; vs++ {
					if jsoniter.Get(data, "dependencies", i).Get("vulnerabilities", v).Get("vulnerableSoftware", vs).Get("software").Get("vulnerabilityIdMatched").ToString() == "true" {
						currentVulnerability.Cpe = jsoniter.Get(data, "dependencies", i).Get("vulnerabilities", v).Get("vulnerableSoftware", vs).Get("software").Get("id").ToString()
						currentVulnerability.VersionFix = jsoniter.Get(data, "dependencies", i).Get("vulnerabilities", v).Get("vulnerableSoftware", vs).Get("software").Get("versionEndExcluding").ToString()
						listVulnerabilities = append(listVulnerabilities, currentVulnerability)
					}
				}
			}

		}
	}
	return listVulnerabilities
}
